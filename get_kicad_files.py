"""
Retrieves and writes all the KiCad 6 files we can find by using the Sourcegraph
API.

SPDX-License-Identifier: MIT
"""

import json
import os
import re

import requests

# you need to get sourcegraph access token and set the SRC_ACCESS_TOKEN env variable
# https://docs.sourcegraph.com/cli/how-tos/creating_an_access_token
token = os.environ["SRC_ACCESS_TOKEN"]

blocklist = (
    # bezier in schematic
    "github.com/Elrori/EBAZ4205/EBAZEXT-V2/",
    "github.com/AlperenAkkuncu/PlainDAQ/PCB/PlainDAQ_v0.1/",
    "github.com/INTI-CMNB/KiDiff/tests/cases/2/a/",
    "github.com/INTI-CMNB/KiDiff/tests/cases/2/b/",
    "github.com/INTI-CMNB/KiDiff/tests/cases/5/b/",
    # unknown format
    "github.com/0xCB-dev/0xcb-1337/rev3.0/",
    # incomplete schematic
    "gitlab.com/kicad/code/kicad/qa/resources/linux/mimeFiles/kicad/schematicFiles/kicadsch.kicad_sch",
    # uses `:` in filenames, that won't work on windows
    "github.com/eddyem/stm32samples/F0:F030,F042,F072/",
    # mirror of gitlab repo
    "github.com/KiCad/kicad-source-mirror/",
    # not actual schematics but `lib_symbols` for some reason
    "github.com/theacodes/blog.thea.codes/srcs/starfish-a-control-board-with-the-rp2040/",
    "github.com/theacodes/kicanvas",
    # invalid label expression using `shape`
    "github.com/Twisted-Fields/rp2040-motor-controller/power_and_cpu_fixed.kicad_sch",
    # git merge conflict marker
    "github.com/zapta/misc/duet3d_accel/",
    "github.com/Jana-Marie/trainlog/",
    # symbolic links
    "github.com/Board-Folk/MegaCD-Main-BD/RAM-Adapter-837-8015-01/MegaCD.kicad_sch",
    "github.com/PySpice-org/kicad-rw/kicad-examples/capacitive-half-wave-rectification-pre-zener/capacitive-half-wave-rectification-pre-zener.kicad_sch",
    # uses "addsublayer" in stackup
    "github.com/eggsampler/PCIeDMA/",
    # no uuid in schematic
    "github.com/Skycode22/HIDs/Not My Projects/Not My Keyboards/hex1/mouse1/",
    "github.com/Ryan-Mirch/Aecerts_Hexapod_V1/test/",
    "github.com/LEDs-by-glowingkitty/GlowTower/elec/layout/default/main-pcb.kicad_sch",
    # old kicad_pcb file
    "github.com/WiseLord/ampcontrol-f103/kicad/ampcontrol-f103",
    # unstable kicad_pcb file
    "github.com/hackclub/sprig/sprig-hax/hardware/DebugShimPCB",
    # invalid kicad_pcb file
    "github.com/artyom-poptsov/SPARC/images/schematics/i2c-pcf8574-lcd-adapter/",
    # invalid pcb file
    "github.com/dawsonjon/101Things/PCB/swiss army pcb",
    # file saved in rescue mode
    "github.com/tjclement/ecsc23-badge/target/",
    #  random effect expression
    "github.com/jotego/jtcores/cores/odyssey/sch/odyssey.kicad_pcb",
    "github.com/jotego/jtcores/cores/flstory/sch/adapter/adapter.kicad_pcb",
    "github.com/AresEnsea/2324_Projet2A_AresCFR/2A/Electronique/Schema Electronique 2223/MainBoard/MainBoard.kicad_pcb",
)


def is_blocked(repo, filepath):
    filename = f"{repo}/{filepath}"
    for b in blocklist:
        if filename.startswith(b):
            return True
    return False


def get_file_list():
    url = "https://sourcegraph.com/.api/search/stream"
    params = {"q": "file:\\.kicad_sch$ select:file.path count:all"}
    headers = {"Authorization": f"token {token}", "Accept": "text/event-stream"}
    res = requests.get(url, params=params, headers=headers, stream=True)
    files = []
    for line in res.iter_lines():
        if line.startswith(b"data: "):
            data = json.loads(line[6:])
            if isinstance(data, list):
                files += [
                    itm for itm in data if "type" in itm and itm["type"] == "path"
                ]
    return files


def request_content(repo, path):
    url = "https://sourcegraph.com/.api/graphql"
    headers = {"Authorization": f"token {token}", "Accept": "application/json"}
    gql = """
        query ($q: String!) {
          search(query: $q, version: V3) {
            results {
              results {
                ... on FileMatch {
                  file {
                    content
                  }
                }
              }
            }
          }
        }
    """
    payload = {
        "query": gql,
        "variables": {"q": f"repo:^{re.escape(repo)}$ file:^{re.escape(path)}$"},
    }
    res = requests.post(url, data=json.dumps(payload), headers=headers)
    res.raise_for_status()
    body = res.json()
    try:
        content = body["data"]["search"]["results"]["results"][0]["file"]["content"]
    except (IndexError, TypeError):
        return None

    return content


def write_contents(files, existing_sch_files: set):
    for i, f in enumerate(files):
        repo = f["repository"]
        sch_path = f["path"]

        if is_blocked(repo, sch_path):
            print(f"Blocked {repo}/{sch_path}")
            continue

        sch_content = request_content(repo, sch_path)
        pcb_content = None
        pro_content = None

        if sch_content is None:
            print(f"Could not retrieve {repo}/{sch_path}")
            continue

        if sch_content in existing_sch_files:
            print(f"({i + 1}/{num_files}) " f"Skipping {repo}/{sch_path}")
            continue

        version_regex = re.compile(r"\(kicad_sch\s+\(version (\d+)\)")
        match = version_regex.match(sch_content)

        if not match:
            print(
                f"({i + 1}/{num_files}) "
                f"Skipping {repo}/{sch_path} due to invalid file type"
            )
            continue
        elif int(match[1]) not in [2022_10_18, 2023_01_21, 2023_11_20]:
            print(
                f"({i + 1}/{num_files}) "
                f"Skipping {repo}/{sch_path} due to older or non-stable version: {match[1]}"
            )
            continue

        full_sch_path = os.path.join("files", repo, sch_path)
        folder = os.path.dirname(full_sch_path)
        os.makedirs(folder, exist_ok=True)
        print(f"({i + 1}/{num_files}) Writing {repo}/{sch_path}")
        with open(full_sch_path, "w", newline="\n") as f:
            f.write(sch_content)
        existing_sch_files.add(sch_content)

        # get the .kicad_pcb with the same name, if available
        pcb_path = re.sub(r"\.kicad_sch$", ".kicad_pcb", sch_path)
        pcb_content = request_content(repo, pcb_path)
        if pcb_content is not None:
            # if there is a pcb there should be a .kicad_pro of that name too
            pro_path = re.sub(r"\.kicad_sch$", ".kicad_pro", sch_path)
            pro_content = request_content(repo, pro_path)

            if pro_content is not None:
                full_pcb_path = os.path.join("files", repo, pcb_path)
                print(f"({i + 1}/{num_files}) Writing {repo}/{pcb_path}")
                with open(full_pcb_path, "w", newline="\n") as f:
                    f.write(pcb_content)

                full_pro_path = os.path.join("files", repo, pro_path)
                print(f"({i + 1}/{num_files}) Writing {repo}/{pro_path}")
                with open(full_pro_path, "w", newline="\n") as f:
                    f.write(pro_content)


def read_existing_files():
    existing_sch_files = []
    for root, _, files in os.walk("files"):
        for file in files:
            if file.endswith(".kicad_sch"):
                path = os.path.join(root, file)
                with open(path) as f:
                    contents = f.read()
                existing_sch_files.append(contents)

    return set(existing_sch_files)


files = get_file_list()
num_files = len(files)
print(f"Found {len(files)} .kicad_sch files")
existing = read_existing_files()
write_contents(files, existing)
